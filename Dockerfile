FROM docker:latest

LABEL maintainer ops@mintlab.nl
LABEL description "Builds Zaaksysteem images"

# docker:latest (or one of its parent images) no longer installs curl, we need
# curl to poke the k8s deploy stage
RUN apk --no-cache --update add curl
